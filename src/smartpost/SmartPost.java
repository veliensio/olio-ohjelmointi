package smartpost;

public class SmartPost {
	/*Stores the smartpost info. For storage class to utilize.
	 * Attributes for this class are filled only by databuilder::getXml 
	 * */
	public String unitCode;
	public String unitCity;
	public String unitAddress;
	public String unitLng;
	public String unitLat;

	
	public SmartPost(String code,String city,String address,String lng,String lat){
		unitCode = code;
		unitCity = city;
		unitAddress = address;
		unitLng = lng;
		unitLat = lat;
		
	}
}
