package smartpost;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.*;
public class DataBuilder {
	
	/*This Class handles the smartpost units. 
	 * Provides methods for downloading and parsing the XML file.
	 * Methods for managing the units are also provided.
	 * */
	
	private ArrayList<SmartPost> unitContainer = new ArrayList<SmartPost>();
	
	private void createUnit(String code,String city,String address,String lng,String lat) {
	//Adds a new smart post to the unit container	
	unitContainer.add(new SmartPost(code,city,address,lng,lat));
	
	}
	
	
    public void getXml() {
    	/*This function downloads the XML file and parses it.
    	 * Data is added to smartpost objects.
    	 * Utilises createUnit method.
    	 * */
    	try {
    		//Download the file and prepare the parser
    		String url = "http://smartpost.ee/fi_apt.xml";
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(new URL(url).openStream());
    		doc.getDocumentElement().normalize();
	
    		//unit data is inside the place element
    		NodeList nList = doc.getElementsByTagName("place");
    				
    		//Loops through all the smartpost units
    		for (int temp = 0; temp < nList.getLength(); temp++) {
    			Node nNode = nList.item(temp);
    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    				Element eElement = (Element) nNode;
    				//every unit is added to the data structure.
    				createUnit(eElement.getElementsByTagName("code").item(0).getTextContent(),eElement.getElementsByTagName("city").item(0).getTextContent(),eElement.getElementsByTagName("address").item(0).getTextContent(),eElement.getElementsByTagName("lng").item(0).getTextContent(),eElement.getElementsByTagName("lat").item(0).getTextContent());
    				
    			}
    		}
    	    } catch (Exception e) {
    		e.printStackTrace();
    	    } 
    }
    
    
	public void dumpUnits() {
		//print all the smart post units for debugging purposes
		 for (SmartPost g: unitContainer) {
			   System.out.print(g.unitCity+ "\n   "); 
		 }
	
	}
	
	public ArrayList<SmartPost> getUnitContainer() {
		return unitContainer;
		
	}
    
	public SmartPost findUnit(String city,String address) {
	//Finds a unit based on the city and street address
		
		 for (SmartPost g: unitContainer) {
			   if(g.unitCity.contains(city)&&g.unitAddress.contains(address)) return g;
		 }
		 
		 return null;
		
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
