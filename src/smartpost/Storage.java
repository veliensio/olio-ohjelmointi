package smartpost;

import java.util.ArrayList;

public class Storage {
	/*This class manages all the created packages and all the items*/
	
	
	private ArrayList<Package> packageContainer = new ArrayList<Package>();
	
	//List of items for create new package ui
	private ArrayList<Item> itemContainer = new ArrayList<Item>();
	
	public Storage(){
		
		//Some pre made items for you
		createItem("DVD-boksi","20*10*10","200",false);
		createItem("Kirja","20*15*5","200",false);
		createItem("Aalto-maljakko","50*50*50","1000",true);
		createItem("Joulukortti","20*20'1","20",false);

	}
	
	
	public void createPackage(Item item,String origin,String dest,String postClass) {
		/*This function wraps the item into a nice package. 
		 * The item is placed into a correct postal class based on its attributes
		 * and the requirements of the postal class. More rules can be added here.
		 * For an example fragile items go instantly to the second class. 
		 * */
		
		if(item.itemFragile == true) {
			packageContainer.add(new SecondClass(item,origin,dest));
		}
		else if(postClass == "1") {
			packageContainer.add(new FirstClass(item,origin,dest));
		}
		else if(postClass == "2") {
			packageContainer.add(new SecondClass(item,origin,dest));
		}
		else if(postClass == "3"){
			packageContainer.add(new ThirdClass(item,origin,dest));
		}
	}
	
	public void createItem(String name,String size,String mass,boolean fragile) {

		itemContainer.add(new Item(name,size,mass,fragile));

	}
	
	public Item getItem(String name) {
		/*This function finds the item from local container based on the name*/
		
		 for (Item g: itemContainer) {
			   if(g.itemName.contains(name)) return g;
		 }
		
	return null;
	}
	
	
	public void dumpPackages() {
		//print all the units for debugging purposes
		 for (Package g: packageContainer) {
			   System.out.print(g.packageItem.itemName+ "\n   "); 
		 }
	
	}
	
	public ArrayList<Package> getPackageContainer() {
		return packageContainer;
		
	}
    
	public ArrayList<Item> getItemContainer() {
		return itemContainer;
		
	}
	
	public Package findPackage(String name,String dest,String origin) {
		/*This function finds the package from local container based on the name*/
	
		 for (Package g: packageContainer) {
			
			   if(g.packageItem.itemName.contains(name)&&g.packageOrigin.contains(origin)&&g.packageDest.contains(dest)) return g;
		 }
		 
		 return null;
		
	}
    
    
	
}
