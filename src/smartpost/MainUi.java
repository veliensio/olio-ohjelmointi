package smartpost;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
 
public class MainUi extends Application {
	
    final ComboBox cbSelectPacket = new ComboBox();
  	 MainApp app = new MainApp();

    @Override
    public void start(Stage stage) {
 
        stage.setTitle("HTML");
        stage.setWidth(500);
        stage.setHeight(800);
        Scene scene = new Scene(new Group());
        VBox root = new VBox();    
        
        //Map View
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        webEngine.load(
                "file:///c:/Users/c0340552/workspace/smartpost/index.html");
        
        //controls for the add location   
        final ComboBox cbSelectLoc = new ComboBox();
  
        Label lblSelectLoc = new Label("Lis�� SmartPost automaatit kartalle: ");	  	
        Button btnMapAdd = new Button();
        btnMapAdd.setText("Lis�� kartalle");
        btnMapAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	/*Adds the selected location to the map.
            	 * */
            	String output = (String) cbSelectLoc.getValue();
            	SmartPost resUnit = app.getUnitPos(output);
               if(resUnit != null){ //Some units are not suitable for the map. (missing info)  	
            	   String address = resUnit.unitAddress+" "+resUnit.unitCode+" "+resUnit.unitCity;
            	   String color ="red";
            	   String dataa ="SmartPost @ "+resUnit.unitAddress +" "+resUnit.unitCity;
            	   String scriptDrawLoc = "document.goToLocation(\""+address+"\",\""+dataa+"\",\""+color+"\")";

            	   webEngine.executeScript(scriptDrawLoc);
               }
            }
        });
        
        //Create a combobox and fill it with the smartpost units (city + address)        

         ObservableList<String> optionsSelectLoc = 
            	    FXCollections.observableArrayList(
   
            	    );
        	
        	ArrayList<SmartPost> unitContainer = app.getSmartPostUnits();
        	
        	for (SmartPost g: unitContainer) {
        		optionsSelectLoc.add(g.unitCity+" "+g.unitAddress); 
        	}
            
        	cbSelectLoc.setItems(optionsSelectLoc);
        	cbSelectLoc.getSelectionModel().select(0);

        	//controls for create package window
            Label lblCreatePacket = new Label("Pakettien luonti ja l�hetys: ");	   	
            Button btnCreatePacket = new Button();
            btnCreatePacket.setText("Luo Paketti");
            btnCreatePacket.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    //creates a new window and all the controls for add packet
                	FlowPane pane1 = new FlowPane();
                	
                	//add controls here
                	
                	 Label lblSelectItem = new Label("Valitse esine: ");	
                	
                     ObservableList<String> optionsSelectItem = 
                     	    FXCollections.observableArrayList(
 
                     	    );
                     
                     ArrayList<Item> items = app.getItems(); //Get the items from the storage class
                    for (Item g: items) {
                    	 optionsSelectItem.add(g.itemName); 
                    	}
                 	
                     final ComboBox cbSelectItem = new ComboBox();
                 	cbSelectItem.setItems(optionsSelectItem);
                 	cbSelectItem.getSelectionModel().select(0);
                 	Label lblNewItem = new Label("Luo uusi esine:                                   "); //using blanks as a substitute for css
                 	Label lblNewItemName = new Label("Nimi:                  ");	
                    TextField tfItemName = new TextField ();
                    Label lblNewItemSize = new Label("Koko (esim. 30*30*30): ");	
                    TextField tfItemSize = new TextField ();
                    Label lblNewItemMass = new Label("Paino (grammoina):");	
                    TextField tfItemMass = new TextField ();
                    
                    CheckBox checkBox = new CheckBox("S�rkyv��?                            ");
                    
               	 Label lblSelectClass = new Label("Valitse luokka: ");	
             	 ObservableList<String> optionsSelectClass = 
                 	    FXCollections.observableArrayList(
                 	    		"1",
                 	    		"2",
                 	    		"3"
                 	    );
                 final ComboBox cbSelectClass = new ComboBox();
             	cbSelectClass.setItems(optionsSelectClass);
             	cbSelectClass.getSelectionModel().select(0);
                    
             	Label lblSelectDest = new Label("Valitse paketin reitti: ");	
            	 ObservableList<String> optionsSelectOrigin = 
                	    FXCollections.observableArrayList();
            	 
             	ArrayList<SmartPost> unitsSelected = app.getSelectedUnits(); //get the unit that appear on the map
            	
            	for (SmartPost g: unitsSelected) {
            		optionsSelectOrigin.add(g.unitCity+" "+g.unitAddress); 
            	}
            	 
                final ComboBox cbSelectOrigin = new ComboBox();
            	cbSelectOrigin.setItems(optionsSelectOrigin); 
            	cbSelectOrigin.getSelectionModel().select(0);
     
       	final ComboBox cbSelectDest = new ComboBox();
            	cbSelectDest.setItems(optionsSelectOrigin); //Same locations appear at both boxes. yes, you can send a packet for yourself.
            	cbSelectDest.getSelectionModel().select(0);
            	
           	 Scene scene1 = new Scene(pane1, 200, 500);
             Stage newStage = new Stage();
            	
                Button btnDispatch = new Button();
                btnDispatch.setText("Paketoi");
                //Item is ready for the wrapping operation
                btnDispatch.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	if(cbSelectOrigin.getValue() != null) { //cant do anything if user didnt add any locations to the map.
                    	String itemName = (String) cbSelectItem.getValue();
                    	
                    	//See if user wanted to create a custom item
                    	if (tfItemName.getText() != null && !(tfItemName.getText().trim().isEmpty())) {
                    		if (tfItemSize.getText() != null && !(tfItemSize.getText().trim().isEmpty())) {
                    			if (tfItemMass.getText() != null && !(tfItemMass.getText().trim().isEmpty())) {
                    				itemName = tfItemName.getText();          
                    				String itemSize = (String)tfItemSize.getText();
                    				String itemMass = (String)tfItemMass.getText();
                    				boolean itemFragile = checkBox.isSelected();
                    				//yes he did. Create the item. In case of missing data we use the default item.
                    				app.createItem(itemName,itemSize,itemMass,itemFragile); 
                    			}
                    		}
                    		
                    	}

                    	
                    	String origin = (String) cbSelectOrigin.getValue();
                    	String dest = (String) cbSelectDest.getValue();
                    	String postClass = (String)cbSelectClass.getValue();
                    	//Finally we got all the info. Create a package
                    	app.createPackage(itemName,origin,dest,postClass);
       
                       //Update main screens combobox to include the created package
                        ObservableList<String> optionsSelectPacket  = 
                         	    FXCollections.observableArrayList(
                         	    );
                     	ArrayList<Package> packages = app.getPackages();
                    	
                    	for (Package g: packages) {
                    		optionsSelectPacket.add(g.packageItem.itemName+" "+g.packageOrigin+" "+g.packageDest); 
                    	}
                        	
                       cbSelectPacket.setItems(optionsSelectPacket);
                       cbSelectPacket.getSelectionModel().select(0);
                       newStage.close();
                    	} 
                    }
                });
                
                
                	//set the order for the elems
                	pane1.getChildren().addAll(lblSelectItem);
                	pane1.getChildren().addAll(cbSelectItem);
                	pane1.getChildren().addAll(lblNewItem);
                	pane1.getChildren().addAll(lblNewItemName);
                	pane1.getChildren().addAll(tfItemName);
                	pane1.getChildren().addAll(lblNewItemSize);
                	pane1.getChildren().addAll(tfItemSize);
                	pane1.getChildren().addAll(lblNewItemMass);
                	pane1.getChildren().addAll(tfItemMass);
                	pane1.getChildren().addAll(checkBox);
                	pane1.getChildren().addAll(lblSelectClass);
                	pane1.getChildren().addAll(cbSelectClass);
                	pane1.getChildren().addAll(lblSelectDest);
                	pane1.getChildren().addAll(cbSelectOrigin);
                	pane1.getChildren().addAll(cbSelectDest);
                	pane1.getChildren().addAll(btnDispatch);
                	
                	//load the window

                     newStage.setScene(scene1);
            
                     newStage.setTitle("Luo uusi paketti");
                     newStage.show();

                }
            });
     
      

            	//MAin UI continues here FYI
            	
                Button btnSendPacket = new Button();
                btnSendPacket.setText("L�het� Paketti");
              
                btnSendPacket.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	if(cbSelectPacket.getValue() != null) {  //Send the package if it was created
                    		String output =(String) cbSelectPacket.getValue();
                    		ArrayList<String> path = app.solvePath(output); //Find out the position data from the selection string.
                    		String color ="\"blue\"";
                    		Package tmp = app.solvePacket(output); //Find out which object selection refers to.
                    	  
                    		int cCode = Integer.parseInt(tmp.packageClass);//get the postal class.
                    
                   
                    		String script = "document.createPath("+path+","+color+","+cCode+")";
                    		//System.out.print(tmp.packageClass+". luokka "+script+"\n");
                    		webEngine.executeScript(script); 
                    	}
                        	
                    }
                });
                
                
  
                Button btnRemoveRoutes = new Button();
                btnRemoveRoutes.setText("Poista Reitit");
                //Delete the routes (In case you don't speak Finnish)
                btnRemoveRoutes.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                    	String script = "document.deletePaths()";
                  	  webEngine.executeScript(script);
             
                    }
                });
        	
   //Set the order for the elements
        root.getChildren().addAll(browser);
        root.getChildren().add(lblSelectLoc);
        root.getChildren().add(cbSelectLoc);
        root.getChildren().add(btnMapAdd);
        root.getChildren().add(lblCreatePacket);
        root.getChildren().add(btnCreatePacket);
        root.getChildren().add(cbSelectPacket);
        root.getChildren().add(btnSendPacket);
        root.getChildren().add(btnRemoveRoutes);
        scene.setRoot(root);
        stage.setScene(scene);
        stage.show();
        
       

    }
    
    
 
    public static void main(String[] args) {
        launch(args);
    }
}


