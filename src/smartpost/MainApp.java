package smartpost;

import java.util.ArrayList;

public class MainApp {
	/* Main functionality of the application.
	 * Utilizes DataBuilder to handle the SmartPost units.
	 * Utilizes Storage to handle packages and items.
	 * 
	 * */
	
	 private DataBuilder dataBuilder;
	 private ArrayList<SmartPost> selectedUnits;
	 private Storage packageStorage;
	 
public MainApp(){
	
	selectedUnits = new ArrayList<SmartPost>();
	
	dataBuilder = new DataBuilder();
    dataBuilder.getXml();
 
    packageStorage = new Storage();

}

public ArrayList<SmartPost>  getSmartPostUnits() {
	
	ArrayList<SmartPost> unitContainer = dataBuilder.getUnitContainer();
	return unitContainer;
}


public SmartPost getUnitPos(String unit) {
	/* Figures out smartpost units exact location based on a city and street address (String unit).
	 * Returns a smartpost object containing the latitude and longitude.
	 * if used from this class might cause duplicates to selectedUnits array.
	 * */
	SmartPost unitPos;

	String[] parts = unit.split(" "); //extract the data
	
	if(parts.length == 2) 
		unitPos = dataBuilder.findUnit(parts[0],parts[1]+" "+"1"); //in case there was no street number. We set it to 1.

	else if(parts.length == 3) 
		unitPos = dataBuilder.findUnit(parts[0],parts[1]+" "+parts[2]);
	
	else return null; //if unit address was really weird. This function is not fool proof without this line.
	
	selectedUnits.add(unitPos); //adds unit to the list of units that appear on the map (Mainly for the create new package ui).
	
	return unitPos;
	
}

public ArrayList<SmartPost>  getSelectedUnits() {
	

	return selectedUnits;
}

public void createPackage(String itemName,String origin,String dest,String postClass){
	
	packageStorage.createPackage(packageStorage.getItem(itemName),origin,dest,postClass); 
	
}

public void createItem(String name,String size,String mass,boolean fragile){
	packageStorage.createItem(name,size,mass,fragile);
}

public ArrayList<Package>  getPackages() {
	
	
	return  packageStorage.getPackageContainer();
}

public ArrayList<Item>  getItems() {
	
	
	return  packageStorage.getItemContainer();
}



public void justaTest() {
	
    this.createPackage("Joulukortti","Lappeenranta","Helsinki","1"); 
    this.createPackage("Kirja","Lappeenranta","Helsinki","2"); 
    this.createItem("risuja","100*20*20","200",false);
    this.createPackage("risuja","Lappeenranta","Helsinki","3"); 
    this.createItem("roskia","100*20*20","200",false);
    this.createPackage("roskia","Lappeenranta","Helsinki","3"); 
    //packageStorage.dumpPackages();
	
}

public Package findPacket(String name,String dest,String origin) {
	return packageStorage.findPackage(name,dest,origin);
}

public Package solvePacket(String output) {
	/* Takes the selection from the select package combobox
	 * And figures out which package was selected.
	 * Returns the object of the selection. In case of empty it return null.
	 * This used to get additional attributes of the selection.
	 */
	
	//extract the addresses.
	String[] parts = output.split(" ");
	String itemName = parts[0];
	String originAdress = parts[1]+" "+parts[2]+" "+parts[3];
	String destAdress = parts[4]+" "+parts[5]+" "+parts[6];
	
	ArrayList<Package> tmpPackets = packageStorage.getPackageContainer();
	
	//find the package and return the object
	 for (Package g: tmpPackets) {
			
		   if(g.packageItem.itemName.contains(itemName)&&g.packageOrigin.contains(originAdress)&&g.packageDest.contains(destAdress)) return g;
	 }
	 

	return null;
	
}



public ArrayList<String>  solvePath(String selectedPackage) {
	/*Gets the select package combobox selection and figures out the path.
	 * Returns arraylist containing the position data in
	 * form : <origin lat>,<origin long>,<dest lat>,<dest long>
	 * */
	ArrayList<String> path = new ArrayList<String>();
	//extract the data.
	String[] parts = selectedPackage.split(" ");
	String itemName = parts[0];
	String originAdress = parts[1]+" "+parts[2]+" "+parts[3];
	String destAdress = parts[4]+" "+parts[5]+" "+parts[6];
	
	//Get the objects containing the position data.
	SmartPost originPlace =  getUnitPos(originAdress);
	selectedUnits.remove(selectedUnits.size() - 1);
	SmartPost destPlace =  getUnitPos(destAdress);
	selectedUnits.remove(selectedUnits.size() - 1); //Method getUnitPos added some unwanted data. Just a hack due to bad design.
	
	path.add(originPlace.unitLat);
	path.add(originPlace.unitLng);
	path.add(destPlace.unitLat);
	path.add(destPlace.unitLng);

	
	return path;
	
}


public void dumpPackages() {
	//print all the smart post units for debugging purposes
	ArrayList<Package> tmp = packageStorage.getPackageContainer();
	
	 for (Package g: tmp) {
		   System.out.print(g.packageDest+ " from app class\n   "); 
	 }

}



}
