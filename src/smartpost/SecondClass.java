package smartpost;

public class SecondClass extends Package {
	
	/*For the second class post. Add Some attributes if needed.
	 * 
	 * */
	  public SecondClass(Item item, String origin,String dest){
		    super(item, origin,dest); //need this to use parents constructor. 
		    packageClass = "2";
		  }
}
