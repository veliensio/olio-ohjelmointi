package smartpost;

public class Item {
	/* Class for the items. Both premade and user made
	 * 
	 * 
	 * */
	
	public String itemName;
	public String itemSize;
	public String itemMass;
	public boolean itemFragile;
	
	
	public Item(String name,String size,String mass,boolean fragile){
		itemName = name;
		itemSize = size;
		itemMass = mass;
		itemFragile = fragile;

	}

}
