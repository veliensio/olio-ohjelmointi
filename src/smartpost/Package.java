package smartpost;

public class Package {
	/*Class for user created packages. Also contains a item selected by the user. 
	 * 
	 * */
	public Item packageItem;
	public String packageOrigin;
	public String packageDest;
	public String packageClass;
	
	
	public Package (Item item, String origin,String dest){
		packageItem = item;
		packageOrigin = origin;
		packageDest = dest;

	}
	
}

